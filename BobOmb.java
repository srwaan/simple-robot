package wrobo;
import robocode.*;
import java.awt.Color;

public class BobOmb extends Robot{
	//We are Sex Bom-Omb, one, two, three, four!
	int direcao = 1;

	public void run() {
		setColors(Color.BLACK,Color.BLACK,Color.BLACK); // body,gun,radar
		while(true) {
			turnRight(30 * direcao);
			
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		//pega o angulo do robo (codigo baseado no RamFire)
		if (e.getBearing() >= 0) 
			direcao = 1;
		else 
			direcao = -1;
		//mantem uma distancia de 200 do robo escaneado
		if (e.getDistance() > 200){
			fire(2);
			ahead(20);			
		} else {
			if (e.getDistance() < 200){
				fire(1);
				back(20);
			}
		}
		scan();
	}
	
	public void onHitByBullet(HitByBulletEvent e) {
		if (e.getBearing() >= 0) 
			direcao = 1;
		else 
			direcao = -1;
		back(100);
	}
	
	public void onHitWall(HitWallEvent e) {
		back(100);
		turnRight(10 * direcao);
		ahead(100);
	}	
}
